import math
import numpy as np
import scipy.stats
from fast_histogram import histogram1d


# This script contains functions that perform feature extraction from a spectrogram.
# The main function is calc_map_aug2(), that subdivides a spectrogram into subregions of
# dimensions given by cents x ms, and then applies a function to each subregion (mean, std deviation, entropy)

def histogram(array, bins=10):
	# Essa função serve pra transformar o formato do fast_histogram pro resultado que eu estava tendo com o
	# numpy.histogram()
	# Pra sua aplicação, se não estiver preocupado com tempo de execução, talvez seja mais simples só
	# usar o numpy.histogram() mesmo
	range = [np.min(array), np.max(array)+0.0001]
	bin_range = (range[1] - range[0]) / 10
	hist = histogram1d(array, 10, range)
	return hist / (np.sum(hist) * bin_range)

def renyi_entropy(tfp_region, alpha=3):  # without normalizaton factor
	# hist = np.histogram(tfp_region, bins=10, density=True)[0]
	hist = histogram(tfp_region, bins=10)
	return (1/(1-alpha)) * np.log2(np.sum(hist ** alpha))
	
def shannon_entropy(tfp_region):
	hist = histogram(tfp_region, bins=10)
	return scipy.stats.entropy(hist)

def find_freq_list(fft_freqs, delta_f_c):
	# Returns the frequency list that determines the musical interval in cents
	# Ex: between fft_freqs[idx_list[i]] and fft_freqs[idx_list[i+1]] there is an interval of delta_f_c cents
	freq_step = fft_freqs[1] - fft_freqs[0]
	
	f1 = 20.0
	f2 = f1 * 2 ** (delta_f_c/1200)
	idx_f1 = find_nearest(fft_freqs, f1)
	idx_f2 = find_nearest(fft_freqs, f2)

	idx_list = []
	idx_list.append(idx_f1)

	if idx_f1 == idx_f2:
		idx_f2 += 1
		
	while idx_f2 < len(fft_freqs):
		idx_list.append(idx_f2)
		idx_f1 = idx_f2
		f1 = fft_freqs[idx_f1]
		f2 = f1 * 2 ** (delta_f_c/1200)  
		idx_f2 = find_nearest(fft_freqs, f2)
		if idx_f1 == idx_f2:
			idx_f2 += 1

	if idx_list[-1] != len(fft_freqs) - 1:
		idx_list.append(len(fft_freqs) - 1)
		
	return idx_list

def extract_feature(spec, kernel_dimensions, type='shannon', n_fft=2048, hop_size=512, sr=44100, alpha=3):
	# extract_feature() divides a spectrogram into subregions of dimensions given by kernel_dimensions, where:
	#   kernel_dimensions[0] is given in ms;
	#   kernel_dimensions[1] is given in cents, and therefore is not linear in frequency.
	# After this division, a mapping is applied for each subregion according to the 'type' keyword, and the
	# resulting matrix is returned.

	idx_list = find_freq_list(fft_frequencies(sr=sr, n_fft=n_fft), kernel_dimensions[1])

	ms_per_frame = hop_size * 1000 / sr
	delta_t = int(np.round(kernel_dimensions[0] / ms_per_frame))

	feature = np.zeros([len(idx_list)-1, spec.shape[1]//delta_t])
	
	j = 0
	j_map = 0

	while j < spec.shape[1] - delta_t:
		for i_map in range(len(idx_list)-1):
			subregion = spec[idx_list[i_map]:idx_list[i_map+1], j:j+delta_t]
			if type=='shannon':
				feature[i_map, j_map] = shannon_entropy(subregion)
			elif type=='renyi':
				feature[i_map, j_map] = renyi_entropy(subregion, alpha=alpha)
		j += delta_t
		j_map += 1
	
	return feature

def find_nearest(array, value):
	# Find the element of 'array' closest to 'value'
	idx = np.searchsorted(array, value, side="left")
	if idx > 0 and (idx == len(array) or math.fabs(value - array[idx-1]) < math.fabs(value - array[idx])):
		return (idx-1)
	else:
		return idx
    
# TIRADO DO LIBROSA
def fft_frequencies(sr=22050, n_fft=2048):
    '''Alternative implementation of `np.fft.fftfreq`
    Parameters
    ----------
    sr : number > 0 [scalar]
        Audio sampling rate
    n_fft : int > 0 [scalar]
        FFT window size
    Returns
    -------
    freqs : np.ndarray [shape=(1 + n_fft/2,)]
        Frequencies `(0, sr/n_fft, 2*sr/n_fft, ..., sr/2)`
    Examples
    --------
    >>> librosa.fft_frequencies(sr=22050, n_fft=16)
    array([     0.   ,   1378.125,   2756.25 ,   4134.375,
             5512.5  ,   6890.625,   8268.75 ,   9646.875,  11025.   ])
    '''

    return np.linspace(0,
                       float(sr) / 2,
                       int(1 + n_fft//2),
    endpoint=True)